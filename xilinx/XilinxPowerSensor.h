#ifndef XILINX_POWER_SENSOR_H
#define XILINX_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace xilinx {
        class XilinxPowerSensor : public PowerSensor {
            public:
                static XilinxPowerSensor* create(
                    int device_number = 0);
        };
    } // end namespace xilinx
} // end namespace powersensor

#endif
