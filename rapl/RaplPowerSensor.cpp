#include "RaplPowerSensor.h"
#include "rapl-read.h"

namespace powersensor {
namespace rapl {

class RaplPowerSensor_ : public RaplPowerSensor {
    private:
        virtual State measure();

        virtual const char* getDumpFileName() {
            return "/tmp/raplpowersensor.out";
        }

        virtual int getDumpInterval() {
            return 500; // milliseconds
        }

        Rapl rapl;
};

RaplPowerSensor* RaplPowerSensor::create()
{
    return new RaplPowerSensor_();
}

State RaplPowerSensor_::measure() {
    State state;
    state.timeAtRead = get_wtime();
    state.joulesAtRead = rapl.measure();
    return state;
}

} // end namespace rapl
} // end namespace powersensor
