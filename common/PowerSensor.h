#ifndef POWER_SENSOR_H_
#define POWER_SENSOR_H_

#include "powersensor-config.h"

#include <memory>
#include <mutex>
#include <fstream>
#include <thread>
#include <vector>

namespace powersensor {

    class State {
        public:
            double timeAtRead;
            double joulesAtRead;
            std::vector<double> misc;
    };

    class PowerSensor {
        public:
            virtual ~PowerSensor();

            virtual State read();

            static double seconds(
                const State &firstState,
                const State &secondState);

            static double Joules(
               const State &firstState,
               const State &secondState);

            static double Watt(
                const State &firstState,
                const State &secondState);

            void startDumpThread(const char *dumpFileName);
            void stopDumpThread();

            void mark(
                const State &startState,
                const State &currentState,
                const char *name = 0,
                unsigned tag = 0) const;

        protected:
            virtual State measure() = 0;

            virtual const char* getDumpFileName() = 0;

            virtual int getDumpInterval() = 0;

            void dump(
                const State &startState,
                const State &firstState,
                const State &secondState);

            static double get_wtime();

        private:
            std::thread dumpThread;
            std::unique_ptr<std::ofstream> dumpFile = nullptr;
            mutable std::mutex dumpFileMutex;
            volatile bool stop = false;
            State previousState;

    };
} // end namespace powersensor

#endif
