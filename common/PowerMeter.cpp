#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>

#include "PowerMeter.h"

void run(powersensor::PowerSensor* sensor, int argc, char* argv[])
{
    char *dumpFileName = std::getenv("POWERSENSOR_DUMPFILE");
    sensor->startDumpThread(dumpFileName);

    if (argc == 1) {
        auto first = sensor->read();
        while (true) {
            auto start = sensor->read();
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            auto end = sensor->read();
            std::cout << powersensor::PowerSensor::seconds(start, end) << " s, ";
            std::cout << powersensor::PowerSensor::Joules(start, end) << " J, ";
            std::cout << powersensor::PowerSensor::Watt(start, end )<< " W, ";
            std::cout << powersensor::PowerSensor::seconds(first, end) << " s (total), ";
            std::cout << powersensor::PowerSensor::Joules(first, end) << " J (total), ";
            std::cout << powersensor::PowerSensor::Watt(first, end )<< " W (average)";
            std::cout << std::endl;
        }
    } else {
        std::stringstream command;
        for (int i = 1; i < argc; i++) {
            if (i > 1) {
                command << " ";
            }
            command << argv[i];
        }
        auto start = sensor->read();
        if (system(command.str().c_str()) !=0) {
            perror(command.str().c_str());
        }
        auto end = sensor->read();
        std::cout << "Runtime: " << powersensor::PowerSensor::seconds(start, end) << " s" << std::endl;
        std::cout << "Joules: " << powersensor::PowerSensor::Joules(start, end) << " J" << std::endl;
        std::cout << "Watt: " << powersensor::PowerSensor::Watt(start, end) << " W" << std::endl;
    }
}