project(likwid)

add_library(
    powersensor-likwid
    OBJECT
    LikwidPowerSensor.cpp
)

target_link_libraries(
    powersensor-likwid
    Threads::Threads
    ${LIKWID_LIBRARY}
)

target_include_directories(
    powersensor-likwid
    PRIVATE
    ${LIKWID_INCLUDE_DIR}
)

install(
    FILES
    LikwidPowerSensor.h
    DESTINATION
    include/powersensor
)

add_executable(
    LikwidPowerMeter
    LikwidPowerMeter.cpp
)

target_link_libraries(
    LikwidPowerMeter
    $<TARGET_OBJECTS:powersensor-common>
    powersensor-likwid
)

install(
    TARGETS
    LikwidPowerMeter
    RUNTIME DESTINATION bin
)

target_link_libraries(
    powersensor
    PUBLIC
    powersensor-likwid
)