#include "ArduinoPowerSensor.h"

/*
    Include original PowerSensor header file
    The _ prefixed aliases prevents class and
    namespace conflicts.
*/
#include POWERSENSOR_HEADER
using _PowerSensor = PowerSensor::PowerSensor;
using _State = PowerSensor::State;
double (&_seconds)(const _State&, const _State&) = PowerSensor::seconds;
double (&_Joules)(const _State&, const _State&, int) = PowerSensor::Joules;


namespace powersensor {
namespace arduino {

class ArduinoPowerSensor_ : public ArduinoPowerSensor {
    public:
        ArduinoPowerSensor_(const char *device);
        ~ArduinoPowerSensor_();

        State measure();

    private:
        virtual const char* getDumpFileName() {
            return "/tmp/arduinopowersensor.out";
        }

        virtual int getDumpInterval() {
            return 1; // milliseconds
        }


        _PowerSensor* _powersensor;
        _State _firstState;
};

ArduinoPowerSensor* ArduinoPowerSensor::create(
    const char *device)
{
    return new ArduinoPowerSensor_(device);
}

ArduinoPowerSensor_::ArduinoPowerSensor_(
    const char *device)
{
    _powersensor = new _PowerSensor(device);
    _firstState = _powersensor->read();
}

ArduinoPowerSensor_::~ArduinoPowerSensor_() {
    delete _powersensor;
}

powersensor::State ArduinoPowerSensor_::measure() {
    _State _state = _powersensor->read();
    powersensor::State state;
    state.timeAtRead   = _seconds(_firstState, _state);
    state.joulesAtRead = _Joules(_firstState, _state, -1);
    return state;
}

} // end namespace arduino
} // end namespace powersensor
