#include "../common/PowerMeter.h"

#include "ROCMPowerSensor.h"

int main(int argc, char *argv[])
{
    auto sensor = powersensor::rocm::ROCMPowerSensor::create(0);
    run(sensor, argc, argv);
    delete sensor;
}