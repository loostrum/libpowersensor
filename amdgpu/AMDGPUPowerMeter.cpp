#include "../common/PowerMeter.h"

#include "AMDGPUPowerSensor.h"

int main(int argc, char *argv[])
{
    auto sensor = powersensor::amdgpu::AMDGPUPowerSensor::create();
    run(sensor, argc, argv);
    delete sensor;
}