#include "DummyPowerSensor.h"

namespace powersensor {

class DummyPowerSensor_ : public DummyPowerSensor {
    private:
        virtual State measure();

        virtual const char* getDumpFileName() {
            return nullptr;
        }

        virtual int getDumpInterval() {
            return 0;
        }
};

DummyPowerSensor* DummyPowerSensor::create()
{
    return new DummyPowerSensor_();
}

State DummyPowerSensor_::measure() {
    State state;
    state.timeAtRead = PowerSensor::get_wtime();
    state.joulesAtRead = 0;
    return state;
}

} // end namespace powersensor
